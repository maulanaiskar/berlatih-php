<?php
function ubah_huruf($kata)
{
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $kataUbah = "";
    for ($i = 0; $i < strlen($kata); $i++) {
        $posisi = strpos($abjad, $kata[$i]);
        $kataUbah .= substr($abjad, $posisi + 1, 1);
    }
    return "$kata = " . $kataUbah;
}

echo ubah_huruf('wow') . "<br>"; // xpx
echo ubah_huruf('developer') . "<br>"; // efwfmpqfs
echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
echo ubah_huruf('keren') . "<br>"; // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu